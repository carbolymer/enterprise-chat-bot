package io.gitlab.carbolymer.enterprisechatbot;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "networks")
@Data
@NoArgsConstructor
public class NetworksConfig {
    private IrcConfig irc;

    @Data
    @NoArgsConstructor
    public static class IrcConfig {
        private String channel;
        private String nick;
        private String server;
        private int port;
        private boolean ssl;
    }
}
