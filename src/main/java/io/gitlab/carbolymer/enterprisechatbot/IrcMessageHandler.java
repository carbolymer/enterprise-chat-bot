package io.gitlab.carbolymer.enterprisechatbot;

import net.engio.mbassy.listener.Handler;
import org.kitteh.irc.client.library.event.channel.ChannelJoinEvent;
import org.kitteh.irc.client.library.event.channel.ChannelMessageEvent;
import org.kitteh.irc.client.library.event.user.PrivateMessageEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IrcMessageHandler {

    private static Logger logger = LoggerFactory.getLogger(IrcMessageHandler.class);

    @Handler
    public void onChannelMessage(ChannelMessageEvent event) {
        logger.info("chan msg: " + event.getMessage());
        event.sendReply("chan reply");
    }

    @Handler
    public void onPrivateMessage(PrivateMessageEvent event) {
        logger.info("priv msg: " + event.getMessage());
        event.sendReply("priv reply");
    }

    @Handler
    public void onJoin(ChannelJoinEvent event) {
        var channel = event.getChannel();
        channel.sendMessage("EHLO \\o/");

    }

}
