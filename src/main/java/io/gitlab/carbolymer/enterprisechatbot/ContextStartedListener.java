package io.gitlab.carbolymer.enterprisechatbot;

import org.kitteh.irc.client.library.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class ContextStartedListener implements ApplicationListener<ContextRefreshedEvent> {

    private Logger logger = LoggerFactory.getLogger(ContextStartedListener.class);

    private final NetworksConfig config;

    public ContextStartedListener(NetworksConfig config) {
        this.config = config;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        logger.info("config: {}", config);

        var client = Client.builder()
                .nick(config.getIrc().getNick())
                .server()
                .host(config.getIrc().getServer())
                .port(config.getIrc().getPort())
                .secure(config.getIrc().isSsl())
                .then()
                .buildAndConnect();
        client.addChannel(config.getIrc().getChannel());
        client.sendMessage(config.getIrc().getChannel(), "hello bot");

        logger.info("Created IRC client");

        client.getEventManager()
                .registerEventListener(new IrcMessageHandler());

    }
}
