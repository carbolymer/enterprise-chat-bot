package io.gitlab.carbolymer.enterprisechatbot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import reactor.core.publisher.Flux;

@Controller
public class RootController {

    private static Logger logger = LoggerFactory.getLogger(RootController.class);


    @GetMapping("/")
    @ResponseBody
    public Flux<String> getRoot() {

        return Flux.just("Alive");
    }
}
