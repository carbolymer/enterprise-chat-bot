package io.gitlab.carbolymer.enterprisechatbot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class EnterpriseChatBotApplication {

    public static void main(String[] args) {
        SpringApplication.run(EnterpriseChatBotApplication.class, args);
    }

}
