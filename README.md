# enterprise-irc-bot

An enterprise-level IRC bot

## Running

Because netty uses `jdk.internal.misc.Unsafe`, it might be necessary to run the application with the following JDK options:
```
-Dio.netty.tryReflectionSetAccessible=true --add-opens java.base/jdk.internal.misc=ALL-UNNAMED
```
